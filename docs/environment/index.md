# Environment

## NERSC User Environment

### Home Directories, Shells and Dotfiles

All NERSC systems use global home directories.  NERSC supports `bash`,
`csh`, and `tcsh` as login shells.  Other shells (`ksh`, `sh`, and
`zsh`) are also available. The default login shell at NERSC is bash.
NERSC does not populate shell initialization files (also known as
dotfiles) on users' home directories.  You can create dotfiles (e.g.,
`~/.bashrc`, `~/.bash_profile`, etc.)  as needed to put your personal shell
modifications.

!!! warning "No more .ext dotfiles at NERSC since February 21, 2020."
	NERSC used to reserve the standard dotfiles (`~/.bashrc`,
    `~/.bash_profile`, `~/.cshrc`, `~/.login`, etc.) for system use so
    that users had to use the corresponding `.ext` files (e.g.,
    `~/.bshrc.ext`, `~/.bash_profile.ext`, etc.)  for their shell
    modifications.  **This is not the case anymore!**  You can modify
    those standard dotfiles for your personal use now.

	The actual dotfile transition occurred during the center maintenance
    on February 21-25, 2020. To mitigate the interruptions to existing
    workloads, we have preserved shell environments by replacing
    dotfiles with template dotfiles that source .ext files. For
    example, if you are an existing user at NERSC, here is how your
    `~/.bashrc` file would look like,

    ```shell
    # begin .bashrc
    if [ -z "$SHIFTER_RUNTIME" ]
    then
        . $HOME/.bashrc.ext
    fi
    # end .bashrc
    ```

	You are recommended to move the contents of your `~/.bashrc.ext` file
    into your `~/.bashrc` file after the transition (and remove the
    .ext files afterwards).


### Changing Default Login Shell

Use [**Iris**](https://iris.nersc.gov/login) to change your default
login shell. Log in, then under the "Details" tab look for the "Server
Logins" section. Click on "Edit" under the "Actions" column.

### Customizing Shell Environment

You can create dotfiles (e.g., `.bashrc`, `.bash_profile`, or
`.profile`, etc) in your `$HOME` directory to put your personal shell
modifications.

!!! note
	On Cori `~/.bash_profile` and `~/.profile` are sourced by
	login shells, while `~/.bashrc` is sourced by most of the shell
	invocations including the login shells.  In general you can put
	the environment variables, such as `PATH`, which are inheritable
	to subshells in `~/.bash_profile` or `~/.profile` and functions
	and aliases in the `~/.bashrc` file in order to make them
	available in subshells.

#### System specific customizations

All NERSC systems share the [Global HOME](../filesystems/global-home);
the same `$HOME` is available regardless of the platform. To make
system specific customizations use the pre-defined environment
variable `NERSC_HOST`.

!!! example

	```shell
	case $NERSC_HOST in
		"cori")
			: # settings for Cori
			export MYVARIABLE="value-for-cori"
			;;
		"datatran")
			: # settings for DTN nodes
			export MYVARIABLE="value-for-dtn"
			;;
		*)
			: # default value for other nodes
			export MYVARIABLE="default-value"
			;;
	esac
	```

#### darshan and altd

NERSC loads a light I/O profiling
tool, [darshan](https://www.mcs.anl.gov/research/projects/darshan/),
and altd (a library tracking tool) on Cori by default.  If you
encounter any problems with them, you can unload them in your
`~/.bash_profile`, or `~/.login` file:

```shell
module unload darshan
module unload altd
```

#### shifter

If you run [shifter](../programming/shifter/how-to-use.md) applications,
you may want to skip the dotfiles.  You can use the
following *if block* in your dotfiles:

```shell
if [ -z "$SHIFTER_RUNTIME" ]; then
	: # Settings for when *not* in shifter
fi
```

#### missing NERSC variables

If any of the NERSC defined environment variables such as `$SCRATCH`,
are missing in your shell invocations, you can add them in your
`~/.bashrc` file as follows:

```shell
if [ -z "$SCRATCH" ]; then
	export SCRATCH=/global/cscratch1/sd/$USER
fi
```

#### crontabs

If you run bash scripts in crontabs, you may want to invoke a login
shell (*`#!/bin/bash -l`*) in order to get the NERSC defined
environment variables, such as `NERSC_HOST`, `SCRATCH`, `CSCRATCH`,
and to get the module command defined.

## NERSC Modules Environment

NERSC uses the [module](http://modules.readthedocs.io) utility to
manage nearly all software. There are two advantages of the module
approach:

1. NERSC can provide many different versions and/or installations of a
   single software package on a given machine, including a default
   version as well as several older and newer version.
2. Users can easily switch to different versions or installations
   without having to explicitly specify different paths. With modules,
   the `MANPATH` and related environment variables are automatically
   managed.

### Module Commands

General usage:

```console
nersc$ module [ switches ] [ subcommand ] [subcommand-args ]
```

Further reading:

 * `module help`
 * `man module`
 * `man modulefile`
 * [Online manual](http://modules.readthedocs.io) (note: some features
   may only be available in later versions than what is installed on
   NERSC systems)

#### Common commands

List currently loaded modules:

```shell
module list
```

List all available modules:

```shell
module avail
```

Show availability of specific module:

```shell
module avail <module-name>
```

Show availability of all modules containing a substring:

```shell
module avail -S <substring>
```

Display what changes are made when a module is loaded:

```shell
module display <module-name>
```

Add a module to your current environment:

```shell
module load <module-name>
```

!!! note
	This command is silent unless there are problems with the
	module.

!!! tip
	If you load then generic name of a module, you will get the
	default version.

	```shell
	module load gcc
	```

	To load a specific version use the full name

	```shell
	module load gcc/8.1.0
	```

Remove module from the current environment:

```shell
module unload <module-name>
```

!!! note
	This command will fail *silently* if the specified module
	is not loaded.

Switch currently loaded module with a new module:

```shell
module swap <old-module> <new-module>
```

### Creating a Custom Module Environment

You can modify your environment so that certain modules are loaded
whenever you log in.

The first option is to use shell commands.

#### bash

In `~/.bash_profile`

```bash
module swap PrgEnv-${PE_ENV,,} PrgEnv-gnu
```

#### csh

In `~/.login`

```csh
set pe = ` echo $PE_ENV | tr "[:upper:]" "[:lower:]" `
module swap PrgEnv-${pe} PrgEnv-gnu
```

#### snapshots

The second option is to use the "snapshot" feature of `modules`.

1. swap and load modules to your desired configuration
2. save a "snapshot" with `module snapshot <snapshot-filename>`

Then at any time later restore the environment with
`module restore <snapshot-filename>`.

### Install Your Own Customized Modules

You can create and install your own modules for your convenience or
for sharing software among collaborators. See the `man modulefile` or
the
[modulefile documentation](https://modules.readthedocs.io/en/latest/modulefile.html#) for
details of the required format and available commands.  These custom
modulefiles can be made visible to the `module` command by `module use
/path/to/the/custom/modulefiles`.

!!! tip
	[Global Common](../filesystems/global-common.md) is the
	recommended location to install software.

!!! note
	Make sure the **UNIX** file permissions grant access to all users who
	want to use the software.

!!! warning
	Do not give write permissions to your home directory to anyone else.

!!! note
	The `module use` command adds new directories before
	other module search paths (defined as `$MODULEPATH`), so modules
	defined in a custom directory will have precedence if there are
	other modules with the same name in the module search paths. If
	you prefer to have the new directory added at the end of
	`$MODULEPATH`, use `module use -a` instead of `module use`.
